# Cryptocurrencies

The cryptocurrencies application is based on bitpanda API.
I am using vanilla PHP 7.1 for the Back-end and jQuery 3.2 and VueJS 2 for the Front-end. 

*The application will cache the cryptocurrency listing data fetched from bitpanda API for one day. 
However, further details for each cryptocurrency won't be cached to maintain the realtime updates.*

The plugins used:

- bootstrap 4.1 (https://getbootstrap.com/): For UI styling
- fontawesome (https://fontawesome.com): For icons

To run the app upload all the files on a machine that has WAMP/XAMP/MAMP/LAMP with PHP 7.1. Then put the app folders and files in the web directory (In my case, I used WAMP and it was C:\wamp64\www). The **public** folder should be triggered by the browser(client) to load the application.

*hints*: 

- In order for bitpanda API to work properly, your server should have SSL certificate.
- I did 2 versions of the front-end of this application. One using VueJs and the other using jQuery. The reason was that the CDN of VueJS makes the 
	application response time not that fast. However, the performance of jQuery is much better. Normally, when accessing the application, it will use VueJS.
	In order to view the jQuery version. you need to set a GET parameter called "use" with the value of "jQuery" like the following:
	
	- VueJs: your-domain
	- jQuery: your-domain?use=jQuery


Here, I have uploaded 2 live demos:

- [DEMO with VueJs][1]
- [DEMO with jQuery][2]

[1]: https://cryptocurrencies.idsarchitects.org
[2]: https://cryptocurrencies.idsarchitects.org?use=jQuery

