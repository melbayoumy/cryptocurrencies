<?php 

class Curl
{

	/**
	 * Perform CURL GET
	 * 
	 * @param string $url
	 * @param array $headers
	 * 
	 * @return string
	 */
	function get($url, $headers = array())
	{ 
		// Initiates the cURL object
		$curl = curl_init();

		// Sets our options array so we can assign them all at once
		$options = [
			CURLOPT_URL => $url,
			CURLOPT_HTTPGET => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => 1,
			CURLOPT_HTTPHEADER => $headers,
		];

		// Assigns our options
		curl_setopt_array($curl, $options);

		// Executes the cURL GET
		$results = curl_exec($curl);

		if ($results === false) {
			throw new Exception(curl_error($curl));
		}

		// Be kind, tidy up!
		curl_close($curl);

		return $results;
	}

	/**
	 * Perform CURL POST
	 * 
	 * @param string $url
	 * @param array $data
	 * @param array $headers
	 * 
	 * @return string
	 */
	function post($url, $data, $headers = array())
	{ 
		// Initiates the cURL object
		$curl = curl_init();

		// Sets our options array so we can assign them all at once
		$options = [
			CURLOPT_URL => $url,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $data,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTPHEADER => $headers,
		];

		// Assigns our options
		curl_setopt_array($curl, $options);

		// Executes the cURL POST
		$results = curl_exec($curl);

		// Be kind, tidy up!
		curl_close($curl);

		return $results;
	}
}
