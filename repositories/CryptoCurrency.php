<?php

require_once(__DIR__ . '/../globals.php');
require_once(__DIR__ . '/Curl.php');

class CryptoCurrency
{
    /**
     * Get all cryptocurrencies - fetch data from API if cache time expired or cache file not found
     * 
     * @return array
     */
    public static function all()
    {
        $cachingTime = time() + (24 * 60 * 60); // Everyday
        $currenciesCacheFile = CACHE_PATH . '/crypto-currency/all.json';
        $apiEndpoint = 'https://api.coinmarketcap.com/v2/listings';

        $currenciesCachedData = self::fetchDataFromCache($currenciesCacheFile);

        if ($currenciesCachedData['error']) {

            return self::fetchDataFromApi($apiEndpoint, $currenciesCacheFile, $cachingTime);

        } else if ($currenciesCachedData['data']['timeout'] < time()) {

            $currenciesApiData = self::fetchDataFromApi($apiEndpoint, $currenciesCacheFile, $cachingTime);

            if ($currenciesApiData['error']) {
                return $currenciesCachedData;
            }
            return $currenciesApiData;
        }

        return $currenciesCachedData;
    }

    /**
     * Get certain cryptocurrency by id
     * 
     * @param string $id
     * @return array
     */
    public static function get($id)
    {
        return self::fetchDataFromApi('https://api.coinmarketcap.com/v2/ticker/' . $id);
    }

    /**
     * Fetch data from bitpanda API and generate cache file on returning data
     * 
     * @param string $endpoint
     * @param string $cacheFile
     * @param integer $cachingTime
     * @return array
     */
    private static function fetchDataFromApi($endpoint, $cacheFile = null, $cachingTime = null)
    {
        $curl = new Curl;

        try {
            $results = json_decode($curl->get($endpoint), true);

            if (count($results['data']) == 0) {
                return [
                    'error' => true,
                    'message' => 'Failed to load data!'
                ];
            }

            if ($cacheFile && $cachingTime) {
                $results['timeout'] = $cachingTime;
                $dataToCache = json_encode($results);
                self::generateCacheFile($cacheFile, $dataToCache);
            }

            return [
                'error' => false,
                'message' => '',
                'data' => $results
            ];
        } catch (Exception $e) {
            return [
                'error' => true,
                'message' => 'Failed to load data!'
            ];
        }
    }

    /**
     * Get data from cache file
     * 
     * @param string $cacheFile
     * @return array
     */
    private static function fetchDataFromCache($cacheFile)
    {
        if (!file_exists($cacheFile)) {
            return [
                'error' => true,
                'message' => 'File does not exist.'
            ];
        }

        $myfile = fopen($cacheFile, "r");
        $results = json_decode(fread($myfile, filesize($cacheFile)), true);
        fclose($myfile);

        return [
            'error' => false,
            'message' => '',
            'data' => $results
        ];
    }

    /**
     * Generate a cache file
     * 
     * @param string $cacheFile
     * @param string $data
     */
    private static function generateCacheFile($cacheFile, $data)
    {
        $myfile = fopen($cacheFile, "w");
        fwrite($myfile, $data);
        fclose($myfile);
    }
}
