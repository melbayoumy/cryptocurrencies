<!doctype html>
<html>
<head>
    <title>Cryptocurrencies</title>

    <meta charset="utf-8" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link href="images/favicon.png" rel="shortcut icon">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="my-4">
            <img src="images/logo.png" width="70px">
            <span class="display-4">Crypto</span><span class="h1">currencies</span>
        <div>
    <?php if ($cryptoCurrencies['error']) : ?>
        <div class="alert alert-danger"><?= $cryptoCurrency['message'] ?></div>
    <?php else : ?>
        <div class="mt-4 list-group"  id="vue-app">
            <?php foreach ($cryptoCurrencies['data']['data'] as $cryptoCurrency) : ?>
                <a href="" @click="showDetails(<?= $cryptoCurrency['id'] ?>)" data-toggle="modal" data-target="#pop-up"
                    class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                    <?= $cryptoCurrency['name'] ?>
                    <span class="badge badge-success badge-pill">
                        <?= $cryptoCurrency['symbol'] ?>
                    </span>
                </a>

                <!-- Modal -->
                <div class="modal fade" id="pop-up">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="modal-title">
                                    <span v-if="!showLoader" class="h5">
                                        {{ response.data.name }}
                                    </span>
                                    <span v-if="!showLoader" class="badge badge-success badge-pill modal-symbol">
                                        {{ response.data.symbol }}
                                    </span>
                                </div>
                                <button class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                        <div class="modal-body">
                            <div v-if="showLoader" class="d-flex justify-content-center">
                                <span>
                                    <i class="fas fa-spinner fa-pulse"></i>
                                </span>
                            </div>
                            <span v-else>
                                <div v-if="response.error" class="text-danger">
                                    {{ response.message }}
                                </div>
                                <ul v-else>
                                    <li v-if="response.data.rank">Rank: {{ response.data.rank }}</li>
                                    <li v-if="response.data.circulating_supply">Circulating supply: {{ response.data.circulating_supply }}</li>
                                    <li v-if="response.data.total_supply">Total supply: {{ response.data.total_supply }}</li>
                                    <li v-if="response.data.max_supply">Max supply: {{ response.data.max_supply }}</li>
                                    <li v-if="response.data.quotes">Quotes (USD):
                                        <ul v-if="response.data.quotes.USD">
                                            <li v-if="response.data.quotes.USD.price">Price: {{ response.data.quotes.USD.price }}</li>
                                            <li v-if="response.data.quotes.USD.volume_24h">Volume (24h): {{ response.data.quotes.USD.volume_24h }}</li>
                                            <li v-if="response.data.quotes.USD.market_cap">Market cap: {{ response.data.quotes.USD.market_cap }}</li>
                                            <li v-if="response.data.quotes.USD.percent_change_1h">Percent change (1h): {{ response.data.quotes.USD.percent_change_1h }}</li>
                                            <li v-if="response.data.quotes.USD.percent_change_24h">Percent change (24h): {{ response.data.quotes.USD.percent_change_24h }}</li>
                                            <li v-if="response.data.quotes.USD.percent_change_7d">Percent change (7d): {{ response.data.quotes.USD.percent_change_7d }}</li>
                                        </ul>
                                    </li>
                                </ul>
                            </span>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">
                                OK
                            </button>
                        </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    <?php endif ?>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="js/main.js"></script>
</body>
</html>