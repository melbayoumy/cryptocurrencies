<!doctype html>
<html>
<head>
    <title>Cryptocurrencies</title>

    <meta charset="utf-8" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link href="images/favicon.png" rel="shortcut icon">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="my-4">
            <img src="images/logo.png" width="70px">
            <span class="display-4">Crypto</span><span class="h1">currencies</span>
        <div>
    <?php if ($cryptoCurrencies['error']) : ?>
        <div class="alert alert-danger"><?= $cryptoCurrency['message'] ?></div>
    <?php else : ?>
        <div class="mt-4 list-group">
            <?php foreach ($cryptoCurrencies['data']['data'] as $cryptoCurrency) : ?>
                <a href="" onClick="showDetails(<?= $cryptoCurrency['id'] ?>)" data-toggle="modal" data-target="#pop-up"
                    class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                    <?= $cryptoCurrency['name'] ?>
                    <span class="badge badge-success badge-pill">
                        <?= $cryptoCurrency['symbol'] ?>
                    </span>
                </a>

                <!-- Modal -->
                <div class="modal fade" data-show="true" id="pop-up">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="modal-title">
                                    <span class="h5 modal-name">
                                    </span>
                                    <span class="badge badge-success badge-pill modal-symbol">
                                    </span>
                                </div>
                                <button class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">
                                OK
                            </button>
                        </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    <?php endif ?>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
    <script src="../js/jQuery.js"></script>
</body>
</html>