<?php

$routes = explode('/', $_SERVER['REQUEST_URI']);

switch ($routes[1]) {
    case 'api':
        require_once(__DIR__ . '/api.php');
        break;

    default:
        require_once(__DIR__ . '/web.php');
}