<?php

require_once(__DIR__ . '/../controllers/CryptocurrencyController.php');

$routes = explode('/', $_SERVER['REQUEST_URI']);

switch ($routes[2]) {
    case 'cryptocurrency':
        echo json_encode(getById($routes[3]));
        break;

    default:
        http_response_code(404);
        die('Not found.');
}
