function showDetails(id) {
    $('.modal-name').html("");
    $('.modal-symbol').html("");
    $('.modal-body').html(`<div v-if="showLoader" class="d-flex justify-content-center">
                                <span><i class="fas fa-spinner fa-pulse"></i></span>
                            </div>`);

    $.ajax(`api/cryptocurrency/${id}`)
        .done(
            (resp) => {
                if (resp.error || !resp.data || !resp.data.data) {
                    $('.modal-body').html("<span class='text-red'>No data found!</span>");
                } else {
                    let currency = resp.data.data
                    $('.modal-name').html(currency.name);
                    $('.modal-symbol').html(currency.symbol);
                    let modalBody = `<ul>`;
                    
                    currency.rank ? modalBody += `<li>Rank: ${currency.rank}</li>` : "";
                    currency.circulating_supply ? modalBody += `<li>Circulating supply: ${currency.circulating_supply}</li>` : "";
                    currency.total_supply ? modalBody += `<li>Total supply: ${currency.total_supply}</li>` : "";
                    currency.max_supply ? modalBody += `<li>Max supply: ${currency.max_supply}</li>` : "";

                    if (currency.quotes && currency.quotes.USD) {
                        modalBody += `<li>Quotes (USD):<ul>`;
                        currency.quotes.USD.price ? modalBody += `<li>Price: ${currency.quotes.USD.price}</li>` : "";
                        currency.quotes.USD.volume_24h ? modalBody += `<li>Volume (24h): ${currency.quotes.USD.volume_24h}</li>` : "";
                        currency.quotes.USD.market_cap ? modalBody += `<li>Market cap: ${currency.quotes.USD.market_cap}</li>` : "";
                        currency.quotes.USD.percent_change_1h ? modalBody += `<li>Percent change (1h): ${currency.quotes.USD.percent_change_1h}</li>` : "";
                        currency.quotes.USD.percent_change_24h ? modalBody += `<li>Percent change (24h): ${currency.quotes.USD.percent_change_24h}</li>` : "";
                        currency.quotes.USD.percent_change_7d ? modalBody += `<li>Percent change (7d): ${currency.quotes.USD.percent_change_7d}</li>` : "";
                        $('.modal-body').append(`
                                </ul>
                            </li>
                        </ul>`);
                    }
                    $('.modal-body').html(modalBody);
                }
            })
        .fail(
            (err) => {
                $('.modal-body').html("<span class='text-red'>Error on loading data!</span>");
            })
}