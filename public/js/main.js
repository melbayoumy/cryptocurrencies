new Vue({
    el: "#vue-app",
    props: ['id'],
    data: {
        response: {
            error: "",
            message: "",
            data: ""
        },
        showLoader: false
    },
    methods: {
        showDetails(id) {
            this.showLoader = true;
            $.ajax(`api/cryptocurrency/${id}`)
                .done(
                    (resp) => {
                        if (resp.error || !resp.data || !resp.data.data) {
                            this.response = {
                                error: true,
                                message: "No data found!"
                            }
                        } else {
                            this.response = {
                                error: false,
                                message: "No data found!",
                                data: resp.data.data
                            }
                        }
                        this.showLoader = false;
                    })
                .fail(
                    (err) => {
                        this.response = {
                            error: true,
                            message: "Error on loading data!"
                        }
                        this.showLoader = true;
                    })
        }
    }
});