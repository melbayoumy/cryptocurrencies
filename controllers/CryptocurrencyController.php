<?php

require_once(__DIR__ . '/../repositories/CryptoCurrency.php');

/**
 * Get all cryptocurrencies
 * 
 * @return view
 */
function getAll()
{
    $cryptoCurrencies = CryptoCurrency::all();

    if (isset($_GET['use']) && $_GET['use'] == 'jQuery') {
        require_once(__DIR__ . '/../views/index-with-jquery.php');
        return;
    }

    require_once(__DIR__ . '/../views/index.php');
}

/**
 * Get cryptocurrency by id
 * 
 * @param integer $id
 * @return json
 */
function getById($id)
{
    if (!isInteger($id)) {
        return [
            'error' => true,
            'message' => 'Invalid ID.'
        ];
    }

    header('Content-type:application/json');
    return CryptoCurrency::get($id);
};

/**
 * Check if input is integer
 * 
 * @param string $number
 * @return boolean
 */
function isInteger($number)
{
    return (is_numeric($number) && substr($number * 10, -1) == 0);
}